`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:42:56 11/21/2018 
// Design Name: 
// Module Name:    PS2Reader 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module PS2Reader(
	input clk, rst, 
	input PS2_DATA, PS2_CLK,
   output reg [7:0] Data,
	output ready
);

// TODO check for parity and enable only when full valid data was recived

reg [3:0] counter;
reg [10:0] tmpData;
reg [1:0] rtmp;

reg [1:0] detNegedge;
wire en;

// negEdge detector on PS2_CLK -> data are readed on neg edge of keyboard clock
// outputs :
// en - 1 clk width signal that edge was detected
always @(posedge clk, posedge rst)
	if(rst)
		detNegedge <= 2'b00;
	else
		detNegedge <= {detNegedge[0], PS2_CLK};
assign en = detNegedge[1] & ~detNegedge[0];
// end of negEdge detector

// 11 bit( frame width) shift register of readed data
// outputs :
// tmpData - update value in shift register
always @(posedge clk, posedge rst) 
	if(rst)
		tmpData <= 11'b0;
	else if(en)
		tmpData <= {PS2_DATA, tmpData[10:1]};
		//tmpData <= {tmpData[9:0], PS2_DATA};
//end of shift register

// counter for readed bits
// count detected PS2_CLK edges
// when full frame was reader reset counter
always @(posedge clk, posedge rst) begin
	if(rst)
		counter <= 4'b0;
	else if(en) begin
		if(counter == 11)
			counter <= 4'b0001; 
			// default value is 0, so when first edge cames it becames 1
			// therefore the edges are actually counted from 1 to 11, 
			//and when reading multiple frames 11 need to change directly to 1 -> see simulation of this module for more information
		else
			counter <= counter + 1;
	end
end
// end of counter

// full frame detector
// when full packet 
assign zero = (counter == 4'd11);
always @(posedge clk, posedge rst)
	if(rst)
		rtmp <= 2'b0;
	else
		rtmp <= {rtmp[0],zero};
assign ready = ~rtmp[1] & rtmp[0];
// end of full frame detector

// Put up collected data
// when full packet was readed expose data
always @(posedge clk, posedge rst) 
	if(rst)
		Data <= 11'd0;
	else if(ready)
		Data <= tmpData[8:1];
		//Data <= tmpData[9:2];
// end of data generator
endmodule
