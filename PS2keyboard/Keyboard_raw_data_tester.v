`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:04:39 11/21/2018 
// Design Name: 
// Module Name:    Keyboard_raw_data_tester 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Keyboard_raw_data_tester(
    input PS2_CLK,
    input PS2_DATA,
    input clk,
    input rst,	 
	 output E,
    output RW,
    output RS,
    output [7:0] DB
    );
	 
wire [7:0] rawData;

wire [7:0] CHAR;
PS2Reader reader(.clk(clk), // input
					.rst(rst),
					.PS2_CLK(PS2_CLK),
					.PS2_DATA(PS2_DATA),
					//.Data(LEDS),
					.Data(rawData), // output
					.ready(ready)
					);
				
reg [1:0] sig;			
always @(posedge clk, posedge rst)
	if(rst)
		sig <= 2'b00;
	else
		sig <= {sig[0],ready};								
assign flag = sig[1];



reg [7:0] previousCode;
reg [7:0] sendCode;
reg convertOpCode;
always @(posedge clk, posedge rst)
	if(rst) begin
		previousCode = 8'h00;
		sendCode = 8'h00;
		end
	else
		if(rawData == 8'hf0)
			sendCode = previousCode;
		else
			previousCode = rawData;
		
					
always @(posedge clk, posedge rst)
	if(rst)
		convertOpCode =1'b0;
	else
		if(flag && rawData == 8'hf0)
			convertOpCode =1'b1;
		else
			convertOpCode =1'b0;
					
ScanCode2AsciiDecoder decoder(.clk(clk), .rst(rst), 
										.signal(convertOpCode), //ready   1'b1
										.scanCode(sendCode), //rawData    8'h15
										.asciiCode(CHAR)
										);


reg [127:0] buffer;
always @(posedge clk, posedge rst) begin
	if(rst)
		buffer <= 127'h0;
	else
		if(ready && rawData != 8'hf0)
			buffer <= { buffer[119:0], CHAR };
end

LCD_Driver #(50000) driver(
                     .rst(rst), // input
                     .clk(clk),// input
                     .bcd_val(buffer),// input  lcdRegister?
                     .E(E),// output
                     .RW(RW),//output
                     .RS(RS),//output
                     .DB_out(DB)
                     );
/*ScanCode2AsciiDecoder2 decoder(.scanCode(rawData), 
										.asciiCode(LEDS));									
*/
//assign rawData = 8'b11100111;

endmodule
