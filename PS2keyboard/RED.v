`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:48:18 10/19/2018 
// Design Name: 
// Module Name:    RED 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module RED(
    input clk,
    input rst,
    input trig,
    output pulse
    );
reg [1:0] tmp;
always@(posedge clk, posedge rst)
	if(rst)
		tmp <= 2'b0;
	else begin
	tmp[0] <= trig;
	tmp[1] <= tmp[0];
	end

assign pulse = tmp[0] & ~tmp[1];



endmodule
