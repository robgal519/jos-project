`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:44:34 10/24/2018 
// Design Name: 
// Module Name:    LCD_dp 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`define Clear 6'b000001
`define Display_on 6'b001110
`define Entry_mode 6'b000110
`define Function_set 6'b111000

module LCD_dp(
    input [127:0] count, // bylo [15:0], bo 4x4bity, bedzie [127:0] bo 16*8bitow
    input [1:0] init_sel,
    input [3:0] mux_sel, //bylo [1:0], jest [3:0] bo 16 wartosci
    input data_sel,
    input DB_sel,
    output reg[7:0] DB_out
    );
	 

	 
reg [7:0] counter_mux;   //bylo [3:0], bo 4 bity, bedzie [7:0], bo 8 bitow
reg [5:0] init_reset_mux;
reg [7:0] data_outx;

always @*
begin
	case(mux_sel)  // byly 4 case'y + 1 default, bedzie 16 case'ow + default
	4'd15 : counter_mux = count[7:0];
	4'd14 : counter_mux = count[15:8];
	4'd13 : counter_mux = count[23:16];
	4'd12 : counter_mux = count[31:24];
	4'd11 : counter_mux = count[39:32];
	4'd10 : counter_mux = count[47:40];
	4'd9 : counter_mux = count[55:48];
	4'd8 : counter_mux = count[63:56];
	4'd7 : counter_mux = count[71:64];
	4'd6 : counter_mux = count[79:72];
	4'd5 : counter_mux = count[87:80];
	4'd4 : counter_mux = count[95:88];
	4'd3 : counter_mux = count[103:96];
	4'd2 : counter_mux = count[111:104];
	4'd1 : counter_mux = count[119:112];
	4'd0 : counter_mux = count[127:120];
	default: counter_mux = 8'd0;
	endcase
end

always @*
begin
	case(init_sel)
	2'b00 : init_reset_mux = `Clear;
	2'b01 : init_reset_mux = `Display_on;
	2'b10 : init_reset_mux = `Entry_mode;
	2'b11 : init_reset_mux = `Function_set;
	default: init_reset_mux = 6'b000000;
	endcase
end


always @*
begin
	case(data_sel) //wtf here?
	1'b0 : data_outx = {2'b00,init_reset_mux};
	1'b1 : data_outx = counter_mux; // byl counter mux + doklejane 4 bity
	default : data_outx = 8'b00000000;
	endcase
end


always @*
begin
	case(DB_sel)
	1'b0 : DB_out = 8'h80; //bylo 8'hcc (dolny prawy), ustawione na 8'h80 (gorny lewy)
	1'b1 : DB_out = data_outx;
	default : DB_out = 8'b00000000;
	endcase
end

endmodule
