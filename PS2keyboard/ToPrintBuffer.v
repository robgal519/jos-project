`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:41:38 12/05/2018 
// Design Name: 
// Module Name:    ToPrintBuffer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ToPrintBuffer(
	input clk, rst,
	input signal,
	input [7:0] letter,
	output reg [127:0] buffer
    );
	 
	 
always @(posedge clk, posedge rst) begin
	if(rst)
		buffer <= 127'd0;
	else
		if(signal)
			buffer <= { buffer[120:0], letter };
end

endmodule
