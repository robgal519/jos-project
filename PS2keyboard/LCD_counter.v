`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    16:00:15 11/07/2018
// Design Name:
// Module Name:    LCD_counter
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_counter #(parameter clk_divider = 50000)(
                   input      rst,
                   input      clk,
                   input      in,
                   output E,
                   output RW,
                   output RS,
                   output [7:0] DB
                   );

   wire [15:0]                 bcd_val;

   LCD_Driver #(clk_divider) driver(
                     .rst(rst), // input
                     .clk(clk),// input
                     .bcd_val(bcd_val),// input
                     .E(E),// output
                     .RW(RW),//output
                     .RS(RS),//output
                     .DB_out(DB)
                     );
   cntBCD BCD_counter(
                      .clk(clk),// input
                      .rst(rst),// input
                      .en(in),// input
                      .out(bcd_val)// output
                      );


endmodule
