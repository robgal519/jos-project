`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:14:18 10/19/2018 
// Design Name: 
// Module Name:    cntBCD 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module cntBCD#(parameter s=4)(
    input clk,
    input rst,
    input en,
    output [s*4-1:0] out
    );
	 
genvar i;
wire [s:0] enn;
wire [s-1:0] ovl1;

wire sig;
RED red(clk, rst, en, sig); 
assign enn[0] = sig;
generate for(i=0;i<s;i=i+1)
	begin: dek
		assign enn[i+1] = enn[i] & ovl1[i];
		
		cnt1dek dek_one(clk, rst, enn[i], ovl1[i], out[4*i+3:4*i]);
	end
endgenerate
endmodule
