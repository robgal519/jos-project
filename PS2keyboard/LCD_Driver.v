`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    15:36:06 11/07/2018
// Design Name:
// Module Name:    LCD_Driver
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_Driver #(parameter clk_divider = 5000)(
                  input            rst,
                  input            clk,
                  input [127:0]     bcd_val,
                  output        E,
                  output        RW,
                  output        RS,
                  output  [7:0] DB_out
                  );

   wire  en, data_sel, DB_sel;
   wire [1:0] init_sel;
	wire [3:0] mux_sel;


   LCD_controller controller(
                             .reset(rst),// input
                             .clk(clk),// input
                             .en(en),// input
                             .init_sel(init_sel),// output
                             .data_sel(data_sel),// output
                             .mux_sel(mux_sel), // output
                             .DB_sel(DB_sel),//output
                             .E(E), // output
									  .RW(RW),// output
									  .RS(RS)// output
									  );
   clk_div #(clk_divider)clock_divider(
                         .clk(clk), // input
                         .rst(rst),// input
                         .en(en)//  output
                         );
   LCD_dp lcd_dp(
                 .DB_sel(DB_sel),//input
                 .mux_sel(mux_sel),// input
                 .data_sel(data_sel),// input
                 .init_sel(init_sel),// input
                 .count(bcd_val),//input
                 .DB_out(DB_out)//output
                 );



endmodule
