`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    21:08:21 11/06/2018
// Design Name:
// Module Name:    lcd_init_regfresh
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module lcd_init_refresh (
                         input            reset,
                         input            clk,
                         input            en,
                         input            mode,
                         input            adr,
                         input            lcd_enable,
                         input            wr_finish,
                         output reg       lcd_finish,
                         output reg       wr_enable,
                         output reg [1:0] init_sel,
                         output reg [3:0] mux_sel // bylo 2 bity, bedzie 4
                         );
   reg [1:0]                              state, nextState;

   localparam idle = 2'b00;
   localparam data = 2'b01;
   localparam data1 = 2'b10;
   localparam endlcd = 2'b11;
	
   always @(posedge clk, posedge reset)
     if(reset)
		init_sel <= 2'b0;
		else if(en)
			case(state)
				idle: init_sel <= 2'b11;
				endlcd: if(~mode) init_sel <= init_sel - 1'b1;
			endcase
			
	 always @(posedge clk, posedge reset)
     if(reset)
		mux_sel <= 4'd0;  //bylo 2'b0
		else if(en)
			case(state)
				idle: mux_sel <= 4'b1111; //bylo 2'b11, czyzby poczatkowa wartosc do dekrementacji?
				endlcd: if(mode) mux_sel <= mux_sel - 1'b1;
			endcase
			
   always @(posedge clk, posedge reset)
     if(reset)
       state <= idle;
     else
       if(en)
         state <= nextState;
			
   always @*
     begin
        lcd_finish = 1'b0;
		  wr_enable = 1'b0; //check
        case(state)
          idle:
            begin
               if(lcd_enable)
                 begin
                    nextState = data;
                 end
               else
                 nextState = idle;
            end // case: idle
          data:
            begin
               wr_enable = 1'b1;
               nextState = data1;
            end
          data1:
            begin
               wr_enable = 1'b0;
               if(wr_finish)
                 nextState = endlcd;
               else
                 nextState = data1;
            end
          endlcd:
            begin
               if(adr)
                 begin
                    lcd_finish = 1'b1;
                    nextState = idle;
                 end
               else
                 begin
                    if(mode)
                      begin
                         if(mux_sel)
                           begin
                              nextState = data;
                           end
                         else
                           begin
                              lcd_finish = 1'b1;
                              nextState = idle;
                           end
                      end // if (mode)
                    else
                      if(init_sel)
                        begin
                           nextState = data;
                        end
                      else
                        begin
                           lcd_finish = 1'b1;
                           nextState = idle;
                        end
                 end // else: !if(adr)
            end // case: endlcd
        endcase // case (state)
     end

endmodule
