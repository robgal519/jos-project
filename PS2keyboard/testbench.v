`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:   16:15:31 11/07/2018
// Design Name:   LCD_counter
// Module Name:   /home/stud2015/5galat/JOS/LCDDisplay/testbench.v
// Project Name:  LCDDisplay
// Target Device:
// Tool versions:
// Description:
//
// Verilog Test Fixture created by ISE for module: LCD_counter
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
////////////////////////////////////////////////////////////////////////////////

module testbench;

   // Inputs
   reg rst;
   reg clk;
   reg in;

   // Outputs
   wire E;
   wire RW;
   wire RS;
   wire [7:0] DB;

   // Instantiate the Unit Under Test (UUT)
   LCD_counter #(10)uut (
                    .rst(rst),
                    .clk(clk),
                    .in(in),
                    .E(E),
                    .RW(RW),
                    .RS(RS),
                    .DB(DB)
                    );

   //make clk signal
   initial begin
      clk = 1'b0;
      forever #10 clk = ~clk;
   end

   // create reset signal
   initial begin
      rst = 1'b0;
      #1 rst = 1'b1;
      #4 rst = 1'b0;
   end

   always @(negedge clk)
	
     #500 in = {$random} %2;


   initial #10000 $finish;
     endmodule // testbench
