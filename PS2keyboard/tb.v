`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////

module tb;

	// Inputs
	reg PS2_CLK;
	reg PS2_DATA;
	reg clk;
	reg rst;

	// Outputs
	wire [7:0] LEDS;

	// Instantiate the Unit Under Test (UUT)
	Keyboard_raw_data_tester uut (
		.PS2_CLK(PS2_CLK), 
		.PS2_DATA(PS2_DATA), 
		.clk(clk), 
		.rst(rst), 
		.E(E),
		.RW(RW),
		.RS(RS),
		.DB(DB)
	);
	
	initial begin
		PS2_DATA = 1;
		PS2_CLK = 1;
		clk = 0;
		rst = 0;
	 end
	
	// initialize system clk
	initial #100 forever #1 clk = ~clk;
	initial begin
		#100;
		rst = 1;
		#5 rst = 0;
	end
	
	// send full packets of data
	////////////////////////////
		
	//set PS2 clock to 11 tics #40 wide
	reg [4:0] it;
	initial begin
	#100;
	for(it = 5'b0; it<5'd22; it = it+1'b1)
		#20 PS2_CLK = ~PS2_CLK;
	PS2_CLK = 1'b1; // left hi to be sure
	end

	// set data to PS2 the data is {1, 55h, 1,1}
	reg [7:0] data;
	reg [3:0] it2;
	initial begin
		#100;
		PS2_DATA = 1'b0;	
		data = 8'h1d;
		#35;
		for(it2 = 0; it2<4'd8; it2 = it2+1'b1) begin
			PS2_DATA = data[it2];
			#40;
		end
	end
	//end of first packet
	
	// send second packet
	initial begin
	#100; // init
	#500; // fist packet
	#500; // blank
	for(it = 5'b0; it<5'd22; it = it+1'b1)
		#20 PS2_CLK = ~PS2_CLK;
	end
		
	initial begin
		#100; // init
		#500; // fist packet
		#500; // blank
		PS2_DATA = 1'b0;	
		data = 8'hf0;
		#35;
		for(it2 = 0; it2<4'd8; it2 = it2+1'b1) begin
			PS2_DATA = data[it2];
			#40;
		end
	end
	
	// end of second packet
	// end of test 
	///////////////////////
	
	initial #1700 $finish;
      
endmodule

