`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    23:05:28 11/06/2018
// Design Name:
// Module Name:    LCD_controller
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module LCD_controller(
                      input         reset,
                      input         clk,
                      input         en,
                      output        E,
                      output        RW,
                      output        RS,
                      output        DB_sel,
                      output  [3:0] mux_sel, // tutaj bedzie 4 bity? :D
                      output        data_sel,
                      output  [1:0] init_sel
                      );

   //wire                                lcd_finish_flag;
   wire                                lcd_enable_flag;
   wire                                adr_flag;
   wire                                mode_;
   wire                                reg_sel_flag;
   wire                                wr_finish_flag;
   wire                                wr_enable_flag;


   main_controller mcontroller(
                              .reset(reset),//input
                              .clk(clk),//input
                              .en(en),//input
                              .lcd_finish(lcd_finish_flag),//input
                              .data_sel(data_sel), // output
                              .DB_sel(DB_sel), //output
                              .reg_sel(reg_sel_flag),
                              .lcd_enable(lcd_enable_flag),//output
                              .adr(adr_flag),// output
                              .mode(mode_)//output
                              );
   lcd_init_refresh refresh(
                            .reset(reset),//input
                            .clk(clk),//input
                            .en(en),//input
                            .mode(mode_),//input
                            .adr(adr_flag),//input
                            .lcd_enable(lcd_enable_flag), //input
                            .wr_finish(wr_finish_flag),//input
                            .lcd_finish(lcd_finish_flag),//output
                            .wr_enable(wr_enable_flag),//output
                            .init_sel(init_sel), // output reg[1:0]
                            .mux_sel(mux_sel)//output reg [1:0]
                            );
   write_cycle write_cycle_(
                            .reset(reset),//input
                            .clk(clk),//input
                            .en(en),//input
                            .reg_sel(reg_sel_flag),//input
                            .wr_enable(wr_enable_flag),//input
                            .wr_finish(wr_finish_flag), // output
                            .E_out(E),//output
                            .rw_out(RW), // output
                            .rs_out(RS)//output
                            );


endmodule
