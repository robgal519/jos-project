`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:49:02 11/07/2018 
// Design Name: 
// Module Name:    clk_div 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clk_div #(parameter div=5000)(
    input clk,
    input rst,
    output reg en
    );

   localparam nff = clogb2(div);
   reg [nff-1:0] cnt;
   always @(posedge clk, posedge rst)
     begin
        if(rst)
          cnt <= 'b0;
        else
          if(cnt == div-1)
            cnt <= 'b0;
          else
            cnt <= cnt + 1'b1;
     end

   always @(posedge clk, posedge rst)
     begin
        if(rst)
          en <= 1'b0;
        else
          if(cnt == div-1)
            en <= 1'b1;
          else
            en <= 1'b0;
     end

   function integer clogb2(input integer v);
     begin
        v = v-1;
        for(clogb2=0;v>0;clogb2 = clogb2 + 1)
          v = v>>1;
     end
   endfunction // clogb2
endmodule // clk_div

