`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:00:39 11/28/2018 
// Design Name: 
// Module Name:    ScanCode2AsciiDecoder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ScanCode2AsciiDecoder(
	input clk, rst,
	input signal,
	input [7:0] scanCode,
	output reg sig,
	output reg [7:0] asciiCode
    );

reg [7:0] ROmem [0:80];
integer i;

initial begin
	for(i=0; i<=80; i = i +1 ) ROmem[i] = 8'h00;  //8'h00
	$readmemh("scan_codes.dat", ROmem);
end

always @(posedge clk, posedge rst)
	if(rst)
		sig <= 1'b0;
	else
		sig <= signal;

always @(posedge clk)
		if(sig) 
			asciiCode = ROmem[scanCode];


endmodule
