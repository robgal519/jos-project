`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:10:26 10/24/2018 
// Design Name: 
// Module Name:    write_cycle 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module write_cycle(
                   input      wr_enable,
                   input      reg_sel,
                   input      reset,
                   input      clk,
                   input      en,
                   output reg wr_finish,
                   output reg rw_out,
                   output reg rs_out,
                   output reg E_out
                   );

   localparam idle=2'b00;
   localparam init=2'b01;
   localparam Eout=2'b10;
   localparam endwr=2'b11;

   reg [1:0]                  st, nst;

   always @(posedge clk, posedge reset)
     if(reset)
       st <= idle;
     else
       if(en)
         st <= nst;

   always@*
     begin
        rw_out = 1'b0;
        rs_out = reg_sel;
        wr_finish = 1'b0;
        nst = idle;
		  E_out = 1'b0; // check
        case(st)
          idle: if(wr_enable) nst = init;
          else nst = idle;
          init: begin
             E_out = 1'b1;
             nst = Eout;
          end
          Eout: begin
             E_out = 1'b1;
             nst = endwr;
          end
          endwr: begin
             E_out = 1'b0;
             wr_finish = 1'b1;
          end
        endcase
     end // always@ *
endmodule // write_cycle
