`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    21:04:19 11/06/2018
// Design Name:
// Module Name:    main_controller
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module main_controller(
                       input  reset,
                       input  clk,
                       input  en,
                       input  lcd_finish, //1 - lck_init_refresh ended its work
                       output reg data_sel,
                       output reg DB_sel,
                       output reg lcd_enable, // Start lcd_init_refresh
                       output reg adr, //tag of addres type for lcd_init_refresh
                       output reg mode, //0 - initialization and addresing, 1 - refresh data
                       output reg reg_sel // RS signal for display to differ command from data
                       );

   localparam LCD_INIT = 0, LCD_REF = 1;

   localparam idle = 3'b000;
   localparam init = 3'b001;
   localparam addr = 3'b010;
   localparam addr1 = 3'b011;
   localparam ref_ = 3'b100;
   localparam ref1 = 3'b101;

   reg [2:0]                  state, nextState;
   always @(posedge clk, posedge reset) begin
      if(reset)
        state <= idle;
      else
        if(en)
          state <= nextState;
   end

   always @*
     begin
	  nextState = idle;
        case(state)
          idle: begin
				lcd_enable=1'b1;
				adr=1'b0;
				DB_sel=1'b1;
				data_sel=1'b0;
				reg_sel=1'b0;
				mode=LCD_INIT;
				nextState = init;
			 end 
          init:begin
				lcd_enable=1'b0;
				adr=1'b0;
				DB_sel=1'b1;
				data_sel=1'b0;
				reg_sel=1'b0;
				mode=LCD_INIT;
			 if(lcd_finish) nextState = addr;
          else
            nextState = init;
				
			 end
          addr: begin
             lcd_enable = 1'b1;
             adr = 1'b1;
             DB_sel = 1'b0;
             nextState = addr1;
				 data_sel=1'b0;
				 reg_sel=1'b0;
				 mode=LCD_INIT;
          end
          addr1: begin
             lcd_enable = 1'b0;
				 adr=1'b1;
				 DB_sel=1'b0;
				 data_sel=1'b0;
				 reg_sel=1'b0;
				 mode=LCD_INIT;
             if(lcd_finish)
               nextState = ref_;
             else
               nextState = addr1;
          end
          ref_: begin
             lcd_enable = 1'b1;
             adr = 1'b0;
             DB_sel = 1'b1;
             data_sel = 1'b1;
             reg_sel = 1'b1;
             mode = LCD_REF;
             nextState = ref1;
          end
          ref1:begin
             lcd_enable = 1'b0;
				 adr=1'b0;
				 DB_sel=1'b1;
             data_sel=1'b1;
             reg_sel=1'b1;
             mode=LCD_REF;
             if(lcd_finish) begin
                reg_sel=1'b0;
                nextState = addr;
             end
             else
               nextState = ref1;
          end
        endcase // case (state)

     end // always @ *
endmodule // main_controller

