`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:56:38 11/28/2018 
// Design Name: 
// Module Name:    ScanCode2AsciiDecoder2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ScanCode2AsciiDecoder2(
    input [7:0] scanCode,
    output reg [7:0] asciiCode
    );

always @* begin
		asciiCode = 8'hff;
	case(scanCode)
		8'h15: asciiCode = 8'h71;	//q
		8'h1D: asciiCode = 8'h77;	//w
		8'h24: asciiCode = 8'h65; //e
		8'h2D: asciiCode = 8'h72;	//r
	endcase
end
	
endmodule
